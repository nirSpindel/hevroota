-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 27, 2018 at 11:51 AM
-- Server version: 10.1.29-MariaDB
-- PHP Version: 7.2.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `hevruta`
--
CREATE DATABASE IF NOT EXISTS `hevruta` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `hevruta`;

-- --------------------------------------------------------

--
-- Table structure for table `classes`
--

CREATE TABLE `classes` (
  `classId` int(10) UNSIGNED NOT NULL,
  `email` varchar(25) NOT NULL,
  `subject` varchar(25) NOT NULL,
  `subtopic` varchar(25) NOT NULL,
  `startDate` date NOT NULL,
  `startTime` time NOT NULL,
  `studentsAmount` varchar(25) NOT NULL,
  `toGender` varchar(25) NOT NULL,
  `status` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `classes`
--

INSERT INTO `classes` (`classId`, `email`, `subject`, `subtopic`, `startDate`, `startTime`, `studentsAmount`, `toGender`, `status`) VALUES
(1, 'asaflariach@gmail.com', 'holidays', 'Passover', '2018-02-06', '20:00:00', '10', 'both', 'onGoing'),
(2, 'asaflariach@gmail.com', 'holidays', 'shvoot', '2018-02-06', '15:00:00', 'noneAmount', 'female', 'future'),
(3, 'asaflariach@gmail.com', 'religion', 'Yom kipur', '2018-02-24', '19:00:00', '5', 'both', 'onGoing'),
(4, 'n_spindel@hotmail.com', 'yom kipur', 'stop meal', '2018-04-26', '00:10:00', '5', 'both', 'onGoing'),
(5, 'n_spindel@hotmail.com', 'yom ha zikaron', 'seremony', '2018-04-27', '00:12:00', '5', 'both', 'onGoing');

-- --------------------------------------------------------

--
-- Table structure for table `feedbacks`
--

CREATE TABLE `feedbacks` (
  `email` varchar(25) NOT NULL,
  `classID` int(10) UNSIGNED NOT NULL,
  `score` tinyint(4) UNSIGNED NOT NULL,
  `type` varchar(8) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `participates`
--

CREATE TABLE `participates` (
  `classID` int(10) UNSIGNED NOT NULL,
  `email` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `firstName` varchar(25) NOT NULL,
  `lastName` varchar(25) NOT NULL,
  `email` varchar(25) NOT NULL,
  `gender` varchar(25) NOT NULL,
  `password` varchar(2000) NOT NULL,
  `username` varchar(25) NOT NULL,
  `dateOfBirth` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Registered users table';

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`firstName`, `lastName`, `email`, `gender`, `password`, `username`, `dateOfBirth`) VALUES
('as', 'la', 'as@walla.com', 'male', '$2y$10$NEqilVMUJgKazgD18kED3OhX3GBz0UpToNQlPHFxXutfazID/Hcg6', 'as', '2018-02-15'),
('asaf', 'lariach', 'asaflariach@gmail.com', 'male', '$2y$10$wyoJncy/UkWxF9LkUFryOODz87h.jJ/3usvn.s.3Un8u7A4p5Xbg.', 'asaf', '2018-02-01'),
('nir', 'spindel', 'n_spindel@hotmail.com', 'male', '$2y$10$GD94f5OKiD3zkG1a0faVX.WLzPAlDXRavQl3pR2ZHf6OrSVDCwjpy', 'ns42', '2018-02-06');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `classes`
--
ALTER TABLE `classes`
  ADD PRIMARY KEY (`classId`),
  ADD KEY `classId` (`classId`);

--
-- Indexes for table `feedbacks`
--
ALTER TABLE `feedbacks`
  ADD KEY `classID` (`classID`),
  ADD KEY `email` (`email`);

--
-- Indexes for table `participates`
--
ALTER TABLE `participates`
  ADD KEY `classID` (`classID`),
  ADD KEY `email` (`email`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`email`),
  ADD UNIQUE KEY `username` (`username`),
  ADD KEY `email` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `classes`
--
ALTER TABLE `classes`
  MODIFY `classId` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `feedbacks`
--
ALTER TABLE `feedbacks`
  ADD CONSTRAINT `feedbacks_ibfk_1` FOREIGN KEY (`classID`) REFERENCES `classes` (`classId`),
  ADD CONSTRAINT `feedbacks_ibfk_2` FOREIGN KEY (`email`) REFERENCES `users` (`email`);

--
-- Constraints for table `participates`
--
ALTER TABLE `participates`
  ADD CONSTRAINT `participates_ibfk_1` FOREIGN KEY (`classID`) REFERENCES `classes` (`classId`),
  ADD CONSTRAINT `participates_ibfk_2` FOREIGN KEY (`email`) REFERENCES `users` (`email`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

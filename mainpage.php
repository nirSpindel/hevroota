<!--Asaf Lariach & Nir Spindel -->
<?php
require_once("dbClass.php"); // include database class //test
session_start();
if(isset($_POST['submit_login'])){
    $db = new dbClass(); // create database object
    $username = $_POST['username']; // catch from the form the username & password
    $password = $_POST['password'];


    // to pervent mysql injection
    $username = stripcslashes($username);
    $password = stripcslashes($password);

    // send the input from user to login via the database
    $user = $db->login($username, $password);

    if($user){
        $_SESSION['user'] = $user;
        header("Location:confirm.php");
        exit;

    }
    else{
        $msg = "Worng username or password";
        echo "<script type='text/javascript'>alert('$msg');</script>";
    }
}
?>

<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="myStyle/mystyle1.css">
    
</head>

<body class="">
<header>
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark" >
        <a class="navbar-brand" href="#">Hevruta</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav">
                <li class="nav-item active">
                    <a class="nav-link" href="#">Home<span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="openclass.php">Start to learn</a>
                </li>
            </ul>
            
            <!-- Button trigger modal -->
            <div class="ml-auto">
                <button type="button" class="btn btn-outline-success my-2 my-sm-0 mr-0" data-toggle="modal" data-target="#exampleModalCenter">
                   Log in
                </button>
                <a id="signbtn" href="signup.php" class="btn btn-outline-success my-2 my-sm-0">Sign up</a>
              </div>
            
                <!-- Modal -->
                <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                  <div class="modal-dialog modal-dialog-centered modal-width" role="document">
                    <div class="modal-content">
                      <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLongTitle">Log In to Your Hevruta Account!</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
                      <div class="modal-body">
                         <form method="post" action="">
                            <div class="form-group">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text"><i class="fas fa-user"></i></div>
                                    </div>
                                    <input type="text" name="username" placeholder="username" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text"><i class="fas fa-key"></i></div>
                                    </div>
                                    <input type="password" name="password" placeholder="password" class="form-control">
                                </div>
                            </div>
                            <div class="form-group text-center">
                                <button name="submit_login" class="btn btn-secondary">Log in</button>
                            </div>
                             <div class="form-group text-center">
                                <span class="tiny-font">forget password? <a href="#">press here</a></span>
                            </div>
                        </form>
                      </div>
                      <div class="modal-footer">
                          <div class="container text-center">
                                <span class="tiny-font">Dont have an account? <a href="#">Sign up</a></span>
                          </div>
                      </div>
                    </div>
                  </div>
                </div>
        </div>
    </nav>
    
    <div class="jumbotron">
        <div class="container text-center">
            <h1 class="text-danger display-2">Welcome to Hevruta</h1>
            <h2 class="text-danger">The place to gather, share and discuss jewish traditions</h2>
            <hr class="my-4"> 
        </div>
    </div>
</header>
    
<div class="container">
    <?php
    if(isset($_SESSION['page'])) {
        if ($_SESSION['page'] == "logout") {
            echo "<div class='alert alert-success' role='alert'>";
            echo "logout successful!";
            echo "</div>";
        }

        if ($_SESSION['page'] == "update") {
            echo "<div class='alert alert-success text-right' role='alert'>";
            echo "המשתמש שלך נמחק בהצלחה";
            echo "</div>";
        }
        unset($_SESSION['page']);
    }
    ?>
</div>
    
    <div class="container">
    <div class="card text-center">
  <div class="card-header">
    <ul class="nav nav-tabs card-header-tabs">
      <li class="nav-item">
        <a class="nav-link active">Live lessons</a>
      </li>
      <li class="nav-item">
        <a class="nav-link">Future lessons</a>
      </li>
    </ul>
  </div>
        
  <div class="card-body" id="table_container">
    
  </div>
</div>
    </div>
    

<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/js/bootstrap.min.js" integrity="sha384-a5N7Y/aK3qNeh15eJKGWxsqtnX/wWdSZSKp+81YjTmS15nvnvxKHuzaWwXHDli+4" crossorigin="anonymous"></script>
<script defer src="https://use.fontawesome.com/releases/v5.0.8/js/all.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="functions/functions.js"></script>
    
</body>
</html>


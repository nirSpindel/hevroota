<!--Asaf Lariach & Nir Spindel -->
<?php
require_once("dbClass.php"); // include database class
require_once("User.php");
session_start();

if(isset($_POST['submit_login'])){//if the user press to login the site
    // create database object
    $db = new dbClass();
    // catch from the form the username & password
    $username = $_POST['username'];
    $password = $_POST['password'];

    // to pervent mysql injection
    $username = stripcslashes($username);
    $password = stripcslashes($password);

    // send the input from user to login via the database
    $user = $db->login($username, $password);

    if($user){//if returned object from the login function all fine
        $_SESSION['user'] = $user;
        header("Location:confirm.php");
        exit;
    }
    else{//else returned null and something worng test
        $msg = "'Wrong username or password!'";
        echo "<script type='text/javascript'>alert('$msg');</script>";
    }
}
?>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/css/bootstrap.min.css" integrity="sha384-Zug+QiDoJOrZ5t4lssLdxGhVrurbmBWopoEl+M6BdEfwnCJZtKxi1KgxUyJq13dy" crossorigin="anonymous">
    <link rel="stylesheet" href="myStyle/mystyle1.css">

</head>

<body class="bg-light">

<header>
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark" >
        <a class="navbar-brand" href="#">Hevruta</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav">
                <li class="nav-item active">
                    <a class="nav-link" href="#">Home<span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="openclass.php">Start to learn</a>
                </li>
            </ul>
            
            <!-- Button trigger modal -->
            <div class="ml-auto">
                <button type="button" class="btn btn-outline-success my-2 my-sm-0 mr-0" data-toggle="modal" data-target="#exampleModalCenter">
                   Log in
                </button>
                <a id="signbtn" href="signup.php" class="btn btn-outline-success my-2 my-sm-0">Sign up</a>
              </div>
            
                <!-- Modal -->
                <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                  <div class="modal-dialog modal-dialog-centered modal-width" role="document">
                    <div class="modal-content">
                      <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLongTitle">Log In to Your Hevruta Account!</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
                      <div class="modal-body">
                         <form method="post" action="">
                            <div class="form-group">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text"><i class="fas fa-user"></i></div>
                                    </div>
                                    <input type="text" name="username" placeholder="username" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text"><i class="fas fa-key"></i></div>
                                    </div>
                                    <input type="password" name="password" placeholder="password" class="form-control">
                                </div>
                            </div>
                            <div class="form-group text-center">
                                <button name="submit_login" class="btn btn-secondary">Log in</button>
                            </div>
                             <div class="form-group text-center">
                                <span class="tiny-font">forget password? <a href="#">press here</a></span>
                            </div>
                        </form>
                      </div>
                      <div class="modal-footer">
                          <div class="container text-center">
                                <span class="tiny-font">Dont have an account? <a href="#">Sign up</a></span>
                          </div>
                      </div>
                    </div>
                  </div>
                </div>
        </div>
    </nav>
    
    <div class="jumbotron">
        <div class="container text-center">
            <h1 class="text-danger display-2">Welcome to Hevruta</h1>
            <h2 class="text-danger">The place to gather, share and discuss jewish traditions</h2>
            <hr class="my-4"> 
        </div>
    </div>
</header>
<div class="container">
    
    <div class="alert alert-danger" role="alert">
        <?php echo ($_SESSION['page'] == 'openclass')? 'You must be logged in to start teaching' : ''; ?>
    </div>
    
    <div class="div-form">
        <div class="container head bg-primary">
            <p class="text-center">Log in</p>
        </div>
        <div class="myform">
            <form class="mt-2" role="form" data-toggle="validator" method="post" action="">

                <div class="form-group text-right">
                    <label for="inputPassword">username</label>
                    <input type="text" name="username" placeholder="username" class="form-control" required>
                </div>

                <div class="form-group text-right">
                    <label for="inputPassword">password</label>
                    <input type="password" name="password" placeholder="password" class="form-control">
                </div>

                <div class="form-group text-center">
                    <button name="submit_login" class="btn btn-primary">Log in</button>
                </div>
            </form>
        </div>
    </div>
</div>



<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/js/bootstrap.min.js" integrity="sha384-a5N7Y/aK3qNeh15eJKGWxsqtnX/wWdSZSKp+81YjTmS15nvnvxKHuzaWwXHDli+4" crossorigin="anonymous"></script>
<script src="https://use.fontawesome.com/cac99f1ae8.js"></script>
<script src="mainpagefuncs.js"></script>

</body>
</html>

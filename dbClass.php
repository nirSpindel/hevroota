<!--Asaf Lariach & Nir Spindel
Class : data-base. -->
<?php

require_once("User.php");

class dbClass {

    private $host; // name of the host(localhost)
    private $db;   // name of the data base (hevrota)
    private $charset;// type of charset (utf8)
    private $user;  // name of user (root)
    private $pass; // root address
    private $opt = array(
        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC
    ); // array with constants to connect the data base
    private $connection;

    /*
     * methood - Constructor
     */
    public function __construct(string $host = "localhost", string $db = "hevruta", string $charset = "utf8", string $user = "root", string $pass = "") {
        $this->host = $host;
        $this->db = $db;
        $this->charset = $charset;
        $this->user = $user;
        $this->pass = $pass;
    }

    /*
     * methood - connect - will connect to the database that we are using
     */
    private function connect() {
        $dsn = "mysql:host=$this->host;dbname=$this->db;charset=$this->charset";
        $this->connection = new PDO($dsn, $this->user, $this->pass, $this->opt);
    }

    /*
     * will disconnect from the data base-after each query
     */
    private function disconnect() {
        $this->connection = null;
    }

    /**
     * 
     * @param type $username - the user name that the user try to login with
     * @param type $password - the password for the username
     * @return \user object to continue to the website
     */
    public function login($username, $password) { // methood to login
        $this->connect(); // first - connect to the db
        $statement = $this->connection->prepare("SELECT * FROM users WHERE username = :username");//prepare the query to execute
        $statement->execute([':username' => $username]); // execute the query
        $user_array = $statement->fetch(PDO::FETCH_ASSOC); // query to array
        $user = new user($user_array['firstName'], $user_array['lastName'], $user_array['email'],
            $user_array['gender'], $user_array['password'], $user_array['username'], 
                         $user_array['dateOfBirth'],$user_array['isAdmin']);

        if(password_verify($password,$user->getPassword())){
            $this->disconnect(); // dissconnect from the database
            return $user; // return the new obje
        }
        else{
            return null;
        }

    }
    
    /**
     * methood to get all the lessons that live broadcast
     * @return classes_array - they array with live lessons information
     */
    public function getCurrentLesson(){
        $this->connect(); // first - connect to the db
        $statement = $this->connection->prepare("SELECT * FROM classes WHERE status = :status");//prepare the query to execute
        $statement->execute([':status' => "onGoing"]); // execute the query
        $classes_array = array();

        while($row = $statement->fetch(PDO::FETCH_ASSOC))
            array_push($classes_array,$row);

        $this->disconnect();
        return $classes_array;
        }


    /**
     * methood to update paswword of the user in the data base
     * @param $email - the primary key to search the specific user in the data base
     * @param $newPassword - the new password to update
     * @return void - not return a value
     */
    public function updatePassword($email, $newPassword){
        $this->connect();
        $statement = $this->connection->prepare("UPDATE users SET password = :newpassword WHERE email = :email");//prepare the query to execute
        $statement->execute([':newpassword' => $newPassword, ':email' => $email]); // execute the query
        $this->disconnect();
    }

    /**
     * methood to update the personal details of the user in the data base
     * @param $firstName - the new first name to update
     * @param $lastName - the new last name to update
     * @param $email - the new email to update
     * @param $dateOfBirth - the new date of birth do update
     * @param $gender - the new gender to update
     * @param $oldEmail - the old email to search the specific user in the data base
     *
     * @return void - not return a value
     */
    public function updateDetails($firstName, $lastName, $email, $dateOfBirth, $gender, $oldEmail){
        $this->connect();
        //prepare the query to execute
        $statement = $this->connection->prepare("UPDATE users SET firstName = :newFname, lastName = :newLname,
                                                  email = :newEmail, dateOfBirth = :newDateOfBirth,gender = :newGender
                                                    WHERE email = :oldEmail");
        // execute the query
        $statement->execute([':newFname' => $firstName, ':newLname' => $lastName,
            ':newEmail' => $email, ':newDateOfBirth' => $dateOfBirth,
            ':newGender' => $gender, ':oldEmail' => $oldEmail]);
        $this->disconnect();
    }

    /** methos to remove user from users table in the data base
     * @param $email - the email to search the specific user in the data base
     * @return void - not return a value
     */
    public function deleteUser($email){
        $this->connect();
        //prepare the query to execute
        $statement = $this->connection->prepare("DELETE FROM users WHERE email = :email ");
        // execute the query
        $statement->execute([':email' => $email]);
        $this->disconnect();
    }

    /**
     * methood to insert new user to data base table (users)
     * @param User $user - the object of the user to insert
     * @return void - not return a value
     */
    public function insertUser(User $user) {

        $this->connect(); // connect to the databse()
        $user->setPassword(password_hash($user->getPassword(),PASSWORD_DEFAULT));//hase the password of the user before insert to DB
        $statement = $this->connection->prepare("INSERT INTO users VALUES (:firstName, :lastName, :email, :gender, :password, :username, :dateOfBirth, :isAdmin)"); // prepare the query with the varibels
        $statement->execute([':firstName' => $user->getFirstName(), ':lastName' => $user->getLastName() // execute the query
            , ':email' => $user->getEmail(), ':gender' => $user->getGender()
            , ':password' => $user->getPassword(), ':username' => $user->getUsername(),
                             ':dateOfBirth'=>$user->getDateOfBirth(), ':isAdmin'=>$user->getAdmin()]);
        $this->disconnect(); // dissconnect from the database
    }

    /**
     * method to insert new class in to the classes table
     * @param $details - array with the details of the class
     * @param $teacherEmail - the tacher of the class email
     * @return void - not return a value
     */
    public function insertClass($details, $teacherEmail){
        // connect to the database
        $this->connect();
        $status = "onGoing";
        // prepare the query
        $statement = $this->connection->prepare("INSERT INTO classes VALUES (NULL, :email, :subject, :subtopic, :startDate, 
            :startTime, :studentsAmount, :toGender, :status)");
        // execute the query
        $statement->execute([':email' => $teacherEmail, ':subject' => $details['subject'], ':subtopic' => $details['subtopic'], ':startDate' => $details['startDate'],
            ':startTime' => $details['startTime'], ':studentsAmount' => $details['amountOfStudents'],
            ':toGender' => $details['toGender'], ':status'=>$status]);
        // dissconnect from the database
        $this->disconnect();
    }
    /**
     * method to get all the classes of teacher from the data base
     * @param $email - the email of th teacher
     * @return array - the array of the classes of the specific teacher
     */
    public function getClassesByTeacher($email){
        $this->connect(); // first - connect to the db
        $statement = $this->connection->prepare("SELECT * FROM classes WHERE email = :email");//prepare the query to execute
        $statement->execute([':email' => $email]); // execute the query
        $classes_array = array();

        while($row = $statement->fetch(PDO::FETCH_ASSOC))
            array_push($classes_array,$row);


        $this->disconnect();
        return $classes_array;


    }

    // public function isAdmin ($user){
    //     $this->connect();// connect to DB
    //     print(" TEST PRINT - SHOULD BE 1 :".$user->admin)
    //     $statement=$this ->connection->prepare("SELECT * FROM users WHERE $user->admin = :1");
    //     $statement->execute(':admin'=>$user->admin);
    //     $admins_array=array();

    //     while($row = $statement->fetch(PDO::FETCH_ASSOC))
    //         array_push($admins_array,$row);

    //     $this->disconnect();


    // }
}

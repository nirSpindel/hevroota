<!--Asaf Lariach & Nir Spindel -->
<?php
require_once("User.php");
require_once("dbClass.php");
session_start();
if(isset($_SESSION['user'])){
    $user = $_SESSION['user'];
}
else{
    $_SESSION['page'] = "update";
    header("Location:mainpage.php");
}
?>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/css/bootstrap.min.css" integrity="sha384-Zug+QiDoJOrZ5t4lssLdxGhVrurbmBWopoEl+M6BdEfwnCJZtKxi1KgxUyJq13dy" crossorigin="anonymous">
    <link rel="stylesheet" href="myStyle/mystyle1.css">

</head>

<body class="bg-light">

<header>
    
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark" >
        <a class="navbar-brand" href="#">Hevruta</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav">
                <li class="nav-item active">
                    <a class="nav-link" href="#">Home<span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="openclass.php">Start to learn</a>
                </li>
        </div>
            <div class="ml-auto">
                <a href="controlPanel.php" class="btn btn-outline-success my-2 my-sm-0 mr-0">Control panel</a>
                <a href="logout.php" class="btn btn-outline-success my-2 my-sm-0">Log out</a>
            </div>  
    </nav>
        
    <div class="jumbotron">
        <div class="container text-center">
            <h1 class="text-danger display-2">Welcome to Hevruta</h1>
            <h2 class="text-danger">The place to gather, share and discuss jewish traditions</h2>
            <hr class="my-4">
            <p>
                <?php
                require_once("User.php");
                $user = $_SESSION['user'];
                echo "<button id='signbtn' class='btn btn-danger btn-lg'> " . "Welcome back, " .$user->getUsername() . "</button>";
                ?>
                
            </p>
        </div>
    </div>
</header>
<div class="container">
    <div class="div-form">
        <div class="container head bg-primary">
            <p class="text-center">עדכון סיסמא</p>
        </div>
        <div class="myform">
            <form class="mt-2" role="form" data-toggle="validator" method="post" action="">

                <div class="form-group text-right">
                    <label for="inputPassword">סיסמא ישנה</label>
                    <input name="old_password" type="password" class="form-control" id="oldpassword" placeholder="סיסמא ישנה" required>
                </div>

                <div class="form-group text-right">
                    <label for="inputPassword">סיסמא חדשה</label>
                    <input name="newpassword" type="password" class="form-control" id="newpassword" placeholder="סיסמא חדשה" required>
                </div>

                <div class="form-group text-center">
                    <button name="updatePassword" type="submit" class="btn btn-primary">עדכן סיסמא </button>
                </div>
            </form>
        </div>
    </div>

    <div class="div-form">
        <div class="container head bg-primary">
            <p class="text-center">עדכון פרטים אישיים</p>
        </div>
        <div class="myform">
            <form class="mt-2" role="form" data-toggle="validator" method="post" action="">

                <div class="form-row text-right">
                    <div class="form-group col-md-6">
                        <label for="inputFname">שם פרטי</label>
                        <input name="fname" type="text" class="form-control" id="inputFname" value="<?php echo $user->getFirstName(); ?>" required>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="inputFname">שם משפחה</label>
                        <input name="lname" type="text" class="form-control" id="inputLname" value="<?php echo $user->getLastName();?>"required>
                    </div>
                </div>

                <div class="form-group text-right">
                    <label for="inputEmail">Email</label>
                    <input name ="email" type="email" class="form-control" id="inputEmail" value="<?php echo $user->getEmail();?>" required>
                </div>

                <div class="form-group text-right">
                    <label for="inputCity">תאריך לידה</label>
                    <input name="birthdate" type="date" class="form-control" id="inputDate" value="<?php echo $user->getDateOfBirth();?>"required>
                </div>

                <div class="form-group text-right ">
                    <div class="custom-control custom-radio custom-control-inline">
                        <input type="radio" id="male" name="gender" value="male" class="custom-control-input" <?php echo ($user->getGender() == 'male')? 'checked' : '' ?>>
                        <label class="custom-control-label" for="male">זכר</label>
                    </div>
                    <div class="custom-control custom-radio custom-control-inline">
                        <input type="radio" id="female" name="gender" value="female" class="custom-control-input" <?php echo ($user->getGender() == 'female')? 'checked' : '' ?>>
                        <label class="custom-control-label" for="female">נקבה</label>
                    </div>
                </div>

                <div class="form-group text-center">
                    <button name="updateDetails" type="submit" class="btn btn-primary">עדכן פרטים</button>
                </div>
            </form>
        </div>
    </div>
    <div class="container">
        <form class="mt-2" role="form" data-toggle="validator" method="post" action="">
            <div class="form-group text-center">
                <button name="deleteUser" type="submit" class="btn btn-primary btn-lg ">מחיקת משתמש</button>
            </div>
        </form>
    </div>
    <div class="container">
        <form class="mt-2" role="form" data-toggle="validator" method="post" action="">
            <div class="form-group text-center">
                <button name="futureClasses" type="submit" class="btn btn-primary btn-lg ">הוצא דו"ח שיעורים עתידיים</button>
            </div>
        </form>
    </div>
</div>



<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/js/bootstrap.min.js" integrity="sha384-a5N7Y/aK3qNeh15eJKGWxsqtnX/wWdSZSKp+81YjTmS15nvnvxKHuzaWwXHDli+4" crossorigin="anonymous"></script>
<script src="https://use.fontawesome.com/cac99f1ae8.js"></script>
<script src="mainpagefuncs.js"></script>

</body>
</html>

<?php
if(isset($_POST['updatePassword'])){//check if the user press to update his password
    $db=new dbClass();
    if(password_verify($_POST['old_password'],$user->getPassword())){//check if the old password match to the hash password
        $newPassword = password_hash($_POST['newpassword'],PASSWORD_DEFAULT);//hash the new password
        $db->updatePassword($user->getEmail(),$newPassword);//send the new password to update in data base, and the mail to find the specific user
        $msg = "הסיסמא שונתה בהצלחה";
    }
    else{
        $msg = "הסיסמא הישנה שהזנת שגויה";
    }
    echo "<script type='text/javascript'>alert('$msg');</script>";
}

if(isset($_POST['updateDetails'])){//check if the user press to update his details
    $db=new dbClass();
    //send details to update, and the old email to find the specific user
    $db->updateDetails($_POST['fname'], $_POST['lname'], $_POST['email'], $_POST['birthdate'], $_POST['gender'], $user->getEmail());

    //update user object to the new details (when refresh the new details show at the form)
    $user->setFirstName($_POST['fname']);
    $user->setLastName($_POST['lname']);
    $user->setEmail($_POST['email']);
    $user->setDateOfBirth($_POST['birthdate']);
    $user->setGender($_POST['gender']);


    echo "<script type='text/javas$msg = \"הפרטים האישיים עודכנו בהצלחה\";cript'>alert('$msg');</script>";
    //set the session array to the new object
    $_SESSION['user'] = $user;
}

if(isset($_POST['deleteUser'])) {//check if the user press to remove his user
    $db=new dbClass();
    //send email to the delete function to find the specific user
    $db->deleteUser($user->getEmail());
    //unset the object index in the session array
    unset($_SESSION['user']);
    $msg = "המשתמש נמחק בהצלחה, רענן את העמוד";
    echo "<script type='text/javascript'>alert('$msg');</script>";
}

if(isset($_POST['futureClasses'])) {//check if the user press to classes report
    $db=new dbClass();

    //send email to the delete function to find the specific user
    $classesArray = $db->getClassesByTeacher($user->getEmail());

    //open file to write in
    $fileToWrite = fopen("classesReport.txt","w");
    fwrite($fileToWrite, "the classes of the teacher " . $user->getUsername() ." is: \r\n\r\n");

    //loop to write every class in row
     for($i=0;$i<count($classesArray);$i++) {
         $results = print_r($classesArray[$i], true); // $results now contains output from print_r
         $toFile="Class subject : " . $classesArray[$i]['subject'] . ", Class subtopic : " . $classesArray[$i]['subtopic']
             .", Class date : " . $classesArray[$i]['startDate'] . ", Class time : " .$classesArray[$i]['startTime']
             . ", To gender : " . $classesArray[$i]['toGender'];
         fwrite($fileToWrite,$toFile . "\r\n");
         fwrite($fileToWrite,"\r\n----------------------------------------------------------------------------\r\n\r\n");
     }

     //close file
      fclose($fileToWrite);

    $msg = "הדוח בוצע בהצלחה!";
    echo "<script type='text/javascript'>alert('$msg');</script>";
}
?>
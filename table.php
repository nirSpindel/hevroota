<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="myStyle/mystyle1.css">

</head>
<body>
    
</body>
</html>
<?php
require_once("dbClass.php");

$db = new dbClass();

//the array of arrays with live lessons information
$clasesArray = $db->getCurrentLesson();

/*
echo "<pre>";
print_r($clasesArray);
echo "</pre>";
*/

//headers of the table cols array
$colsArr = array("#","Teacher", "Subject", "Subtopic", "Time left", "Slots left", "join");

//indexes of valus we need from the classes informations array
$indexArr = array("email" ,"subject", "subtopic", "startTime", "startDate" );

$rows = count($clasesArray); // amout of tr 
$cols = count($colsArr);// amjount of td 

echo "<table class='table  table-striped'>";

echo "<tr>";//start row of headers

//loop to make headers row
for($i = 0; $i < $cols; $i++){
    echo"<th scope='col'>" . $colsArr[$i] . "</th>";
} 

echo "</tr>";//end rows of headers

//rows loop
for($i = 0; $i < $rows; $i++){ 
    echo "<tr>";//start row
    
    echo "<th scope='row'> " . $i . "</th>";//seriel number of the row in table
        
        //cols loop
        for($j=1;$j < $cols - 1;$j++){ 
               echo "<td>" . $clasesArray[$i][$indexArr[$j-1]] ."</td>"; 
        } 
    
    echo "<td> <button type='button' class='btn-outline-dark btn-sm m-0'> Join lesson </button> </td>";
    
    echo "</tr>"; //end row
} 

echo "</table>";

?>
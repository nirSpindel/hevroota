<!--Asaf Lariach & Nir Spindel -->
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/css/bootstrap.min.css" integrity="sha384-Zug+QiDoJOrZ5t4lssLdxGhVrurbmBWopoEl+M6BdEfwnCJZtKxi1KgxUyJq13dy" crossorigin="anonymous">
    <link rel="stylesheet" href="myStyle/mystyle1.css">

</head>

<body class="bg-light">

<header>
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark" >
        <a class="navbar-brand" href="#">Hevruta</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav">
                <li class="nav-item active">
                    <a class="nav-link" href="#">Home<span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="openclass.php">Start to learn</a>
                </li>
        </div>
            <div class="ml-auto">
                <a href="controlPanel.php" class="btn btn-outline-success my-2 my-sm-0 mr-0">Control panel</a>
                <a href="logout.php" class="btn btn-outline-success my-2 my-sm-0">Log out</a>
            </div>  
    </nav>
        
    <div class="jumbotron">
        <div class="container text-center">
            <h1 class="text-danger display-2">Welcome to Hevruta</h1>
            <h2 class="text-danger">The place to gather, share and discuss jewish traditions</h2>
            <hr class="my-4">
            <p>
                <?php
                require_once("User.php");
                session_start();
                $user = $_SESSION['user'];
                echo "<button id='signbtn' class='btn btn-danger btn-lg'> " . "Welcome back, " .$user->getUsername() . "</button>";
                ?>
                
            </p>
        </div>
    </div>
</header>
<div class="container">

</div>



<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/js/bootstrap.min.js" integrity="sha384-a5N7Y/aK3qNeh15eJKGWxsqtnX/wWdSZSKp+81YjTmS15nvnvxKHuzaWwXHDli+4" crossorigin="anonymous"></script>
<script src="https://use.fontawesome.com/cac99f1ae8.js"></script>
<script src="mainpagefuncs.js"></script>

</body>
</html>



<!--Asaf Lariach & Nir Spindel -->
<?php
require_once("User.php");
require_once("dbClass.php");
session_start();
if(isset($_SESSION['user'])){
    $user = $_SESSION['user'];
}
else{
    $_SESSION['page'] = "update";
    header("Location:mainpage.php");
}
?>


<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/css/bootstrap.min.css" integrity="sha384-Zug+QiDoJOrZ5t4lssLdxGhVrurbmBWopoEl+M6BdEfwnCJZtKxi1KgxUyJq13dy" crossorigin="anonymous">
    <link rel="stylesheet" href="myStyle/mystyle1.css">

</head>

<body class="bg-light">

<header>
    
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark" >
        <a class="navbar-brand" href="#">Hevruta</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav">
                <li class="nav-item active">
                    <a class="nav-link" href="#">Home<span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="openclass.php">Start to learn</a>
                </li>
        </div>
            <div class="ml-auto">
                <a href="controlPanel.php" class="btn btn-outline-success my-2 my-sm-0 mr-0">Control panel</a>
                <a href="logout.php" class="btn btn-outline-success my-2 my-sm-0">Log out</a>

                <?php
                    require_once("User.php");
                    $user = $_SESSION['user'];
                    if($user->getAdmin())
                        echo "<a href='adminPage.php' class='btn btn-outline-success my-2 my-sm-0'>Admin page</a>";
                ?>

            </div>  
    </nav>
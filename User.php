<!--Asaf Lariach & Nir Spindel
Class : User. -->
<?php

class User {

    private $firstName; // first name of user
    private $lastName;  // last name of user
    private $email;     //email of user
    private $gender;    //gender of user
    private $password;  // password of user
    private $username;  // username of user
    private $dateOfBirth;// birthday of user
    private $isAdmin;


    /*
     * methood Constructor
     */
    public function __construct($firstName, $lastName, $email, $gender, $password, $username, $dateOfBirth, $isAdmin) {
        $this->setFirstName($firstName); // all the fields will send to their setters
        $this->setLastName($lastName);   // each field to his own setter
        $this->setEmail($email);
        $this->setGender($gender);
        $this->setPassword($password);
        $this->setUsername($username);
        $this->setDateOfBirth($dateOfBirth);
        $this->setAdmin($isAdmin);
    }


    /*
     * methood to string
     */
    public function toString() { // will echo out all the details of user

        return "First name : " . $this->firstName . "<br>Last name : " . $this->lastName . "<br>User name : " . $this->username . "<br>Email : " . $this->email . "<br>Gender : " . $this->gender . "<br>Date of birth : " . $this->dateOfBirth;
    }

    public function setFirstName($firstName) {
            $this->firstName = $firstName;// set the first name
    }

    public function getFirstName() {
        return $this->firstName;// return the first name
    }

    public function setLastName($lastName) {
            $this->lastName = $lastName; // setthelast name
    }

    public function getLastName() {
        return $this->lastName; // return the last name
    }

    public function setEmail($email) {
            $this->email = $email; // set the email
    }

    public function getEmail() {
        return $this->email; // return the email
    }

    public function setGender($gender) {
        $this->gender = $gender; // set the gender
    }

    public function getGender() {
        return $this->gender; // return the gender
    }

    public function setUsername($username) {
        $this->username = $username; // set the username
    }

    public function getUsername() {
        return $this->username; // return the username
    }

    public function setPassword($password) {
        $this->password = $password; // set the password
    }

    public function getPassword() {
        return $this->password; // return the password
    }

    public function setDateOfBirth($dateOfBirth) {
        $this->dateOfBirth = $dateOfBirth; // set the date of birth
    }

    public function getDateOfBirth() {
        return $this->dateOfBirth; // return the dateof birth
    }
    
    public function setAdmin($admin) {
        $this->isAdmin = $admin;
    }
    
    public function getAdmin() {
        return $this->isAdmin;
    }
}

?>
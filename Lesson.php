<!--Asaf Lariach & Nir Spindel
Class : Lesson. -->
<?php

class Lesson {

	private $classId; // uniqe ID for class
	private $email; // teacher's email
	private $subject; // subject of class
	private $subtopic; // subtopic of lesson
	private $startDate; // starting date
	private $startTime; // starting time 
	private $studentAmount; // participation limit
	private $toGender; // fits for 
	private $status; // status of lesson


		/*
		* methood Constructor
		*/
	public function __construct($$email,$subject,$subtopic,$startDate,$startTime,$studentAmount,$toGender,$status){

		$this->setEmail($email);
		$this->setSubject($subject);
		$this->setSubtopic($subtopic);
		$this->setStartDate($setStartDate);
		$this->setStartTime($setStartTime);
		$this->setStudentAmount($studentAmount);
		$this->setToGender($toGender);
		$this->setStatus($status);

	}


	public function setClassId($classId) {
            $this->classId = $classId;// set the first name
    }

    public function getclassid() {
        return $this->classId;// return the first name
    }

    public function setEmail($email) {
            $this->email = $email;// set the email 
    }

    public function getEmail() {
        return $this->email;// return the mail
    }


    public function setSubject($subject) {
            $this->subject = $subject;// set the subject 
    }

    public function getSubject() {
        return $this->subject;// return the subject
    }


    public function setSubtopic($subtopic) {
            $this->subtopic = $subtopic;// set the subtopic
    }

    public function getSubtopic() {
        return $this->subtopic;// return the subtopic
    }


    public function setStartDate($startDate) {
            $this->startDate = $startDate;// set the start date 
    }

    public function getStartDate() {
        return $this->startDate;// return the start date
    }


    public function setStartTime($startTime) {
            $this->startTime = $startTime;// set the starttime
    }

    public function getStartTime() {
        return $this->startTime;// return the start time
    }


    public function setStudentAmount($studentAmount) {
            $this->studentAmount = $studentAmount;// set the student amount
    }

    public function getStudentAmount() {
        return $this->studentAmount;// return the student amount
    }


    public function setToGender($toGender) {
            $this-> = $toGender;// set the gender 
    }

    public function getToGender() {
        return $this->toGender;// return the gender
    }


    public function setStatus($status) {
            $this->status = $status;// set the status of lesson
    }

    public function getStatus() {
        return $this->status;// return the status of lesson
    }

}

?>
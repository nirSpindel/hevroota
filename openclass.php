<!--Asaf Lariach & Nir Spindel -->
<?php
require_once("User.php");
require_once("dbClass.php");
session_start();

if(isset($_SESSION['user'])){
    $user = $_SESSION['user'];
}
else{
    $_SESSION['page'] = "openclass";
    header("Location:login.php");
}

/*
	function to make a select box from array
	*/
function selectitems($items, $selected = 0) : string {
    echo "<pre>";
    print_r($items);
    echo "</pre>";
    $text = "";
    foreach($items as $k=>$v){
        if ($k === $selected)
            $ch = " selected";
        else
            $ch = "";
        $text .= "<option$ch value='$v'>$v</option>\n";
    }
    return $text;
}
$subjects = array("holidays", "assimilation", "Hebrew language", "religion");
$subtopics = array("Passover", "purim", "hanuka", "shvoot", "Rosh A shana", "Yom kipur");
?>



<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/css/bootstrap.min.css" integrity="sha384-Zug+QiDoJOrZ5t4lssLdxGhVrurbmBWopoEl+M6BdEfwnCJZtKxi1KgxUyJq13dy" crossorigin="anonymous">
    <link rel="stylesheet" href="myStyle/mystyle1.css">
</head>

<body class="bg-light">

<header>
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark" >
        <a class="navbar-brand" href="#">Hevruta</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav">
                <li class="nav-item active">
                    <a class="nav-link" href="#">Home<span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="openclass.php">Start to learn</a>
                </li>
        </div>
            <div class="ml-auto">
                <a href="controlPanel.php" class="btn btn-outline-success my-2 my-sm-0 mr-0">Control panel</a>
                <a href="logout.php" class="btn btn-outline-success my-2 my-sm-0">Log out</a>
            </div>  
    </nav>
        
    <div class="jumbotron">
        <div class="container text-center">
            <h1 class="text-danger display-2">Welcome to Hevruta</h1>
            <h2 class="text-danger">The place to gather, share and discuss jewish traditions</h2>
            <hr class="my-4">
            <p>
                <?php
                require_once("User.php");
                $user = $_SESSION['user'];
                echo "<button id='signbtn' class='btn btn-danger btn-lg'> " . "Welcome back, " .$user->getUsername() . "</button>";
                ?>
            </p>
        </div>
    </div>
</header>
<div class="container">
    <div class="div-form">
        <div class="container head bg-primary">
            <p class="text-center">Open lesson</p>
        </div>
        <div class="myform">
            <form class="mt-2" role="form" data-toggle="validator" method="post" action="">
                <div class="form-group">
                    <label for="inputSubject">Subject</label>
                    <select name="subject" class="form-control" id="inputSubject">
                        <?=selectitems($subjects, $_POST['subject'])?>
                    </select>
                </div>

                <div class="form-group">
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="inputSubtopic">Sub topic</label>
                            <select name="subtopic" class="form-control" id="inputSubject">
                                <?=selectitems($subtopics, $_POST['subtopic'])?>
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="inputSubtopic">Free text</label>
                            <input name="subtopic2 "type="text" class="form-control" id="inputSubtopic2" placeholder="Sub topic">
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="startDate">Start date:</label>
                    <input name="startDate" type="date" class="form-control" id="startDate" required>
                </div>

                <div class="form-group">
                    <label for="startTime">Start time:</label>
                    <input name="startTime" type="time" class="form-control" id="startTime" required>
                </div>

                <div class="form-group">
                    <label for="amountOfStudents">Maximum amount of students:</label>
                    <select name="amountOfStudents" class="form-control" id="capacity">
                        <option value="noneAmount">ללא</option>
                        <option value="2">2</option>
                        <option value="5">5</option>
                        <option value="10">10</option>
                        <option value="15">15</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="gender">Recomended to gender:</label>
                </div>
                <div class="form-group ">
                    <div class="custom-control custom-radio custom-control-inline">
                        <input type="radio" id="male" name="toGender" value="male" class="custom-control-input" checked>
                        <label class="custom-control-label" for="male">Male</label>
                    </div>
                    <div class="custom-control custom-radio custom-control-inline">
                        <input type="radio" id="female" name="toGender" value="female" class="custom-control-input">
                        <label class="custom-control-label" for="female">Female</label>
                    </div>
                    <div class="custom-control custom-radio custom-control-inline">
                        <input type="radio" id="both" name="toGender" value="both" class="custom-control-input">
                        <label class="custom-control-label" for="both">Both</label>
                    </div>
                </div>

                <div class="form-group text-center">
                    <button name="open_class" type="submit" class="btn btn-primary">Open class</button>
                </div>
            </form>
        </div>
    </div>
</div>


<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/js/bootstrap.min.js" integrity="sha384-a5N7Y/aK3qNeh15eJKGWxsqtnX/wWdSZSKp+81YjTmS15nvnvxKHuzaWwXHDli+4" crossorigin="anonymous"></script>
<script src="https://use.fontawesome.com/cac99f1ae8.js"></script>
<script src="mainpagefuncs.js"></script>

</body>
</html>

<?php
if (isset($_POST['open_class'])){
    $db = new dbClass();
    $db->insertClass($_POST, $user->getEmail());
    
}

?>